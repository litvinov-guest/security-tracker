A DSA is needed for the following source packages in old/stable. The specific
CVE IDs do not need to be listed, they can be gathered in an up-to-date manner from
https://security-tracker.debian.org/tracker/source-package/SOURCEPACKAGE
when working on an update.

Some packages are not tracked here:
- Linux kernel (tracking in kernel-sec repo)
- Embargoed issues continue to be tracked in separate file.

To pick an issue, simply add your uid behind it.

If needed, specify the release by adding a slash after the name of the source package.

--
389-ds-base (fw)
  Thorsten Alteholz proposed an update
--
evolution
--
faad2
  not yet fixed upstream
--
glusterfs
--
ghostscript (carnil)
  we will potentially update again to 9.27 (9.27~dfsg-0+deb9u1)
--
graphicsmagick
--
koji
--
libidn
  santiago proposed debdiffs for jessie and stretch
--
libpng1.6
  wait for final patch
--
libssh2
--
linux
  Wait until more issues have piled up
--
mercurial

nss
  Roberto proposed an update including fixes for CVE-2018-12404 and CVE-2018-18508
--
pdns (seb)
  2019-03-31 Chris Hofstaedtler prepared debdiff
--
putty
  Maintainer preparing updates
--
ruby2.5
--
simplesamlphp
--
smarty3
--
sox
--
sssd
  Maintainer prepared an update and proposed debdiff, acked for upload, but update needs further testing before release.
--
wordpress
--
xen
--
