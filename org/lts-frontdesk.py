#!/usr/bin/env python3

import sys
import datetime

HEADER = """
Presentation
------------

The LTS frontdesk handles:

 * CVE triaging:
   https://wiki.debian.org/LTS/Development#Triage_new_security_issues

 * Making sure that queries on debian-lts@lists.debian.org get an answer.

Who is in charge ?
------------------
"""

LINE = """From {0.day:02d}-{0.month:02d} to {1.day:02d}-{1.month:02d}:"""


def main(year):
    print(HEADER.strip())
    print()

    for x, y in generate_weeks(int(year)):
        print(LINE.format(x, y))


def generate_weeks(year):
    dt = datetime.date(year, 1, 1)

    while dt.year == year:
        if dt.weekday() == 0:
            yield (dt, dt + datetime.timedelta(days=6))
        dt += datetime.timedelta(days=1)


if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
